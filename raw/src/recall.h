/*
 * recall.h
 *
 *  Created on: 31 d�c. 2015
 *      Author: Modou
 */

#include "precision.h"

#ifndef METRICS_RECALL_H_
#define METRICS_RECALL_H_

namespace metrics {
class Recall : public Precision {
protected:
	virtual double score(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend = 30){
		if(test.empty() || preds.empty() || nbItems_to_recommend == 0)
			return 0.0;
		return intersect(preds, test, nbItems_to_recommend) / test.size();
	}

public:
	Recall(std::string name, int nbRecommendations=5) : Precision(name, nbRecommendations){}

	virtual ~Recall(){}
};
}

#endif /* METRICS_RECALL_H_ */
