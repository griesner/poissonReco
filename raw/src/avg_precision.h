/*
 * avg_precision.h
 *
 *  Created on: 23 jan. 2016
 *      Author: Modou
 */

#include "metric.h"

#ifndef METRICS_AVG_PRECISION_H_
#define METRICS_AVG_PRECISION_H_

namespace metrics {
class AvgPrecision : public Metric {
protected:
	virtual double score(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend = 30){
		if(test.empty() || preds.empty() || nbItems_to_recommend == 0)
			return 0.0;
		int nb = 0;
		double sum = 0;
		for(int i=0; i < std::min((int)preds.size(), nbItems_to_recommend); i++)
			if(contains(preds[i], test)){
				nb++;
				sum += (nb / (i+1));
			}
		return (nb == 0)? 0.0 : (sum / nb);
	}

public:
	AvgPrecision(std::string name, int nbRecommendations=5) : Metric(name, nbRecommendations){}

	virtual ~AvgPrecision(){}
};
}

#endif /* METRICS_AVG_PRECISION_H_ */
